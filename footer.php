        <a name="footer"></a>
        <footer class="c-layout-footer c-layout-footer-4 c-bg-footer-8">
            <div class="c-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 c-footer-4-p-right">
                            <div class="c-content-title-1">
                                <h3 class="c-font-uppercase c-font-bold c-font-white c-border">JAN
                                    <span class="c-theme-font">GO</span>
                                </h3>
                            </div>
                            <p class="c-about"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed elit diam nonummy ad minim veniam quis nostrud exerci et tation diam nisl ut aliquip exit commodo consequat euismod tincidunt ut laoreet dolore magna aluam. </p>
                            <div class="c-links">
                                <ul class="c-nav">
                                    <li>
                                        <a class="c-active c-theme-border c-theme-font" href="#">Home</a>
                                    </li>
                                    <li>
                                        <a href="#">About</a>
                                    </li>
                                    <li>
                                        <a href="#">Work</a>
                                    </li>
                                    <li>
                                        <a href="#">Careers</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact</a>
                                    </li>
                                </ul>
                            </div>
                            <p class="c-contact"> 25, Lorem Lis Street, Orange C, California, US
                                <br> Phone: 800 123 3456
                                <br> Fax: 800 123 3456
                                <br> Skype: jango.inc </p>
                            <ul class="c-socials">
                                <li>
                                    <a href="#">
                                        <i class="icon-social-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-social-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-social-youtube"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-social-dribbble"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6 c-footer-4-p-left">
                            <div class="c-feedback">
                                <h3 class="c-font-thin">Contact Us</h3>
                                <form action="#">
                                    <input type="text" placeholder="Your Name" class="form-control">
                                    <input type="text" placeholder="Your Email" class="form-control">
                                    <textarea rows="8" name="message" placeholder="Write comment here ..." class="form-control"></textarea>
                                    <button type="submit" class="btn c-btn-white c-btn-border-2x c-btn-uppercase btn-lg c-btn-bold c-btn-square">Send</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- END: LAYOUT/FOOTERS/FOOTER-8 -->
        <!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
        <div class="c-layout-go2top">
            <i class="icon-arrow-up"></i>
        </div>
        <!-- END: LAYOUT/FOOTERS/GO2TOP -->
        <!-- BEGIN: LAYOUT/BASE/BOTTOM -->
        <!-- BEGIN: CORE PLUGINS -->
        <!--[if lt IE 9]>
	<script src="../assets/global/plugins/excanvas.min.js"></script> 
	<![endif]-->
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jquery.easing.min.js" type="text/javascript"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/reveal-animate/wow.js" type="text/javascript"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/base/js/scripts/reveal-animate/reveal-animate.js" type="text/javascript"></script>
        <!-- END: CORE PLUGINS -->
        <!-- BEGIN: LAYOUT PLUGINS -->
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
        <!-- END: LAYOUT PLUGINS -->
        <!-- BEGIN: THEME SCRIPTS -->
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/base/js/components.js" type="text/javascript"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/base/js/components-shop.js" type="text/javascript"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/base/js/app.js" type="text/javascript"></script>
        <script>
            $(document).ready(function()
            {
                App.init(); // init core    
            });
        </script>
        <!-- END: THEME SCRIPTS -->
        <!-- END: LAYOUT/BASE/BOTTOM -->
    </body>

</html>