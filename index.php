<?php get_header(); ?>

   <div class="c-layout-page">
            <!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
            <div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
                <div class="container">
                    <div class="c-page-title c-pull-left">
                        <h3 class="c-font-uppercase c-font-sbold">Footer - Compact</h3>
                    </div>
                    <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
                        <li>
                            <a href="javascript:;">Features</a>
                        </li>
                        <li>/</li>
                        <li>
                            <a href="javascript:;">Footers</a>
                        </li>
                        <li>/</li>
                        <li>
                            <a href="footer-2.html#footer">Footer 2</a>
                        </li>
                        <li>/</li>
                        <li class="c-state_active">Jango Components</li>
                    </ul>
                </div>
            </div>
            <!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
            <!-- BEGIN: PAGE CONTENT -->
            <!-- BEGIN: CONTENT/PRODUCTS/PRODUCT-1 -->
            <div class="c-content-box c-size-md c-bg-white c-no-bottom-padding">
                <div class="container">
                    <div class="c-content-product-1 c-opt-1">
                        <div class="c-content-title-1">
                            <h3 class="c-center c-font-uppercase c-font-bold">Learn More About JANGO</h3>
                            <div class="c-line-center"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 wow animate slideInUp">
                                <div class="c-media">
                                    <img src="assets/base/img/content/misc/jango-intro-3.png" alt="" /> </div>
                            </div>
                            <div class="col-md-8">
                                <div class="c-body">
                                    <ul class="c-row">
                                        <li class="wow animate fadeInUp">
                                            <h4>Code Quality</h4>
                                            <p>Lorem ipsum dolor sit amet consectetuer adipiscing elit sed diam nonummy nibh euismod</p>
                                        </li>
                                        <li class="wow animate fadeInUp">
                                            <h4>Angular JS Support</h4>
                                            <p>Lorem ipsum dolor sit amet consectetuer adipiscing elit sed diam nonummy nibh euismod</p>
                                        </li>
                                    </ul>
                                    <ul class="c-row">
                                        <li class="wow animate fadeInUp">
                                            <h4>Every Growing Unique Layouts</h4>
                                            <p>Lorem ipsum dolor sit amet consectetuer adipiscing elit sed diam nonummy nibh euismod</p>
                                        </li>
                                        <li class="wow animate fadeInUp">
                                            <h4>Fully Mobile Optimized</h4>
                                            <p>Lorem ipsum dolor sit amet consectetuer adipiscing elit sed diam nonummy nibh euismod</p>
                                        </li>
                                    </ul>
                                    <button type="button" class="btn btn-md c-btn-border-2x c-btn-square c-btn-regular c-btn-uppercase c-btn-bold c-margin-b-100">Learn More</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: CONTENT/PRODUCTS/PRODUCT-1 -->
            <!-- BEGIN: CONTENT/BARS/BAR-3 -->
            <div class="c-content-box c-size-md c-bg-dark">
                <div class="container">
                    <div class="c-content-bar-3">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="c-content-title-1">
                                    <h3 class="c-font-uppercase c-font-bold">DEDICATED SUPPORT</h3>
                                    <p class="c-font-uppercase">JANGO comes with top-of-the-line support teams to ensure that we provide the best experience for our customers</p>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-offset-2">
                                <div class="c-content-v-center" style="height: 90px;">
                                    <div class="c-wrapper">
                                        <div class="c-body">
                                            <button type="button" class="btn btn-md c-btn-square c-btn-border-2x c-theme-btn c-btn-uppercase c-btn-bold">Get Support</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: CONTENT/BARS/BAR-3 -->
            <!-- BEGIN: CONTENT/MISC/SERVICES-1 -->
            <div class="c-content-box c-size-md c-bg-grey-1  ">
                <div class="container">
                    <div class="c-content-feature-2-grid" data-auto-height="true" data-mode="base-height">
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="c-content-feature-2" data-height="height">
                                    <div class="c-icon-wrapper">
                                        <div class="c-content-line-icon c-theme c-icon-screen-chart"></div>
                                    </div>
                                    <h3 class="c-font-uppercase c-font-bold c-title">Web Design</h3>
                                    <p>Lorem ipsum consectetuer dolore elit diam</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="c-content-feature-2" data-height="height">
                                    <div class="c-icon-wrapper">
                                        <div class="c-content-line-icon c-theme c-icon-support"></div>
                                    </div>
                                    <h3 class="c-font-uppercase c-font-bold c-title">Mobile Apps</h3>
                                    <p>Lorem ipsum consectetuer dolore elit diam</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="c-content-feature-2" data-height="height">
                                    <div class="c-icon-wrapper">
                                        <div class="c-content-line-icon c-theme c-icon-comment"></div>
                                    </div>
                                    <h3 class="c-font-uppercase c-font-bold c-title">Consulting</h3>
                                    <p>Lorem ipsum consectetuer dolore elit diam</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="c-content-feature-2" data-wow-delay1="2s" data-height="height">
                                    <div class="c-icon-wrapper">
                                        <div class="c-content-line-icon c-theme c-icon-bulb"></div>
                                    </div>
                                    <h3 class="c-font-uppercase c-font-bold c-title">Campaigns</h3>
                                    <p>Lorem ipsum consectetuer dolore elit diam</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="c-content-v-center c-theme-bg wow bounceInUp" data-wow-delay1="2s" data-height="height">
                                    <div class="c-wrapper">
                                        <div class="c-body c-padding-20 c-center">
                                            <h3 class="c-font-19 c-line-height-28 c-font-uppercase c-font-white c-font-bold"> Providing the best possible service to our customers without a breaking a sweat </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="c-content-feature-2" data-wow-delay1="2s" data-height="height">
                                    <div class="c-icon-wrapper">
                                        <div class="c-content-line-icon c-theme c-icon-globe"></div>
                                    </div>
                                    <h3 class="c-font-uppercase c-font-bold c-title">Hosting</h3>
                                    <p>Lorem ipsum consectetuer dolore elit diam</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php get_footer(); ?>